# -*- coding: utf-8 -*-
"""
Created on Sun Jul 28 16:22:32 2019

@author: paula.guarido
"""

import sys
import os
import json
import operator
import csv
import pandas as pd
import time

path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')

try:
    fichero = open(path + '\procesados\procesado.json', 'r')
    print 'fichero leido'
    #fichero.close()
except IOError:
    print 'error de lectura 1'

tweets_data = []

for tweet_line in fichero:
    tweet = json.loads(tweet_line)
    tweets_data.append(tweet)

tweet_line = None

#Función para asignar el tipo de tweet en las tablas tw_hashtags y tw_msg
def type_tw(tweet):
    #caso1
    if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(1)
    #caso2
    elif 'quoted_status' not in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(2)
    #caso3
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(3)
    #caso4
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(4)

print 'tipo de tweet asignado'
#TABLA tw_msg
tw_msg = pd.DataFrame()
tw_msg['id_str_msg'] = map(lambda tweet: tweet['id_str'], tweets_data)
tw_msg['timestamp_ms'] = map(lambda tweet: tweet['timestamp_ms'], tweets_data)
tw_msg['lang'] = map(lambda tweet: tweet['lang'], tweets_data)
tw_msg['id_str_user'] = map(lambda tweet: tweet['user']['id_str'], tweets_data)
#estos dos son siempre 0
tw_msg['retweet_count'] = map(lambda tweet: tweet['retweet_count'], tweets_data)
tw_msg['favorite_count'] = map(lambda tweet: tweet['favorite_count'], tweets_data)
tw_msg['retweet_count_rt'] = map(lambda tweet: tweet['retweeted_status']['retweet_count'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['retweet_count_fv'] = map(lambda tweet: tweet['retweeted_status']['favorite_count'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['retweeted_status_id_str'] = map(lambda tweet: tweet['retweeted_status']['id_str'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['quoted_status_id_str'] = map(lambda tweet: tweet['quoted_status']['id_str'] if 'quoted_status' in tweet.keys() else None, tweets_data)
#tw_msg['date'] = map(lambda tweet: time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')),tweets_data)
tw_msg['date'] = map(lambda tweet: time.strftime('%Y-%m-%d', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')),tweets_data)
#Recogemos el texto del tweet normal RT o QT
"""
1. Normal. sin RT o QE --> texto del tweet --> tweet['text']
2. RT. sin modificar el original --> texto del RT --> tweet['retweeted_status']['text']
3. QE. modificando el original --> 2 textos, tweet['quoted_status']['text'], tweet['text']
4. RT+QE. RT de un QE --> igual que 3
"""
tw_msg['text_m'] = map(lambda tweet: tweet['text'] if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys() else '', tweets_data)
tw_msg['text_rt'] = map(lambda tweet: tweet['retweeted_status']['text'] if 'retweeted_status' in tweet.keys() else '', tweets_data)
tw_msg['text_qt'] = map(lambda tweet: tweet['quoted_status']['text'] if 'quoted_status' in tweet.keys() else '', tweets_data)
# Generamos el texto a enviar a MC
tw_msg['text_mc'] = tw_msg['text_m'] +' '+ tw_msg['text_rt'] +' '+ tw_msg['text_qt']
# Indicamos si es un tweet a MC
##tw_msg['is_mc'] = False
##tw_msg.is_mc.ix[tw_msg['id_str_msg'].isin(tw_hashtags.id_str_msg[tw_hashtags.id_tw_user_screen_name != '0'])] = True
###tw_msg.is_mc.ix[tw_msg['id_str_msg'].isin(tw_user_mentions.id_str_msg[tw_user_mentions.id_tw_user_screen_name != '0'])] = True
#Tipo tw
tw_msg['type'] = map(lambda tweet: type_tw(tweet), tweets_data)

print 'tabla tw_msg_creada'
# Guardamos la salida en formato excel y csv
tw_msg.to_excel(path + '\procesados\excel\\tw_msg.xlsx')
tw_msg.to_csv(path + '\procesados\csv\\tw_msg.csv', encoding = 'utf-8')

print 'tabla tw_msg_a_excel'