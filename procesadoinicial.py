# -*- coding: utf-8 -*-
"""
Created on Sat May 25 15:33:12 2019

@author: paula.guarido
"""

import os
import json
import csv

path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')
#path = 'C:\TFG\ficherosdatos'
#os.chdir(path)

try:
    with open(path + '\output.txt', 'r') as file:
        #procesado=open('C:\Axians\OneDrive - VINCI Energies\Axians\UNIR\Asignaturas\TFG\files\procesados\procesado.json', 'w')
        procesado=open(path + '\procesados\procesado.json', 'w')
        for line in file:
            if line.strip('\r\r\n'):
                procesado.write(line.rstrip('\r\r\n'))
                procesado.write('\n')
        print 'archivo procesado'
        procesado.close()
#except IOError:
    #print 'error de lectura 1'
    #print IOError.message
except Exception as e:
    print 'error de lectura 1'
    print (e)
    
    
try:
    fichero = open(path + '\procesados\procesado.json', 'r')
    print 'fichero leido'
except IOError:
    print 'error de lectura 2'
#ARRAY
tweets_data = []
#DICCIONARIO no se pude iterar con un for
tweet_hash = {}
tweet_user = {}

for tweet_line in fichero:
    
    tweet = json.loads(tweet_line)
    tweets_data.append(tweet)
    
    if "entities" in tweet.keys():
        
        hashtags = tweet["entities"]["hashtags"]
        for ht in hashtags:
            if ht != None:
                if ht["text"].encode("utf-8") in tweet_hash.keys():
                    tweet_hash[ht["text"].encode("utf-8")] += 1
                else:
                    tweet_hash[ht["text"].encode("utf-8")] = 1
                    
        user_mentions = tweet["entities"]["user_mentions"]
        for um in user_mentions:
            if um != None:
                if um["screen_name"].encode("utf-8") in tweet_user.keys():
                    tweet_user[um["screen_name"].encode("utf-8")] += 1
                else:
                    tweet_user[um["screen_name"].encode("utf-8")] = 1
                    
# se graban en un csv los diccionarios con usuarios y hashtags
with open(path + '\procesados\usuarios.csv', 'wb') as output:
    write = csv.writer(output)
    for key, value in tweet_user.iteritems():
        write.writerow([key, value])
        
with open(path + '\procesados\hashtag.csv', 'wb') as output:
    write = csv.writer(output)
    for key, value in tweet_hash.iteritems():
        write.writerow([key, value])
            
                    
                
        