# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 17:54:26 2019

@author: paula.guarido
"""
import os
import matplotlib.pyplot as plt
from matplotlib import dates
plt.style.use('ggplot')
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU
import pandas as pd

path =  os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')

#cargar tabla
day_mentions_d = pd.read_excel(path + '\procesados\excel\\day_mentions_d.xlsx', index_col = False, dtype={'id_str_msg':str})
day_mentions_d = day_mentions_d.set_index('id_day_mention')

#agrupamos por usuario
by_user = day_mentions_d.groupby('id_tw_user_screen_name')

#cambiamos valores
day_mentions_d.type[day_mentions_d.type == 1] = 'u'
day_mentions_d.type[day_mentions_d.type == 2] = 'd'
day_mentions_d.type[day_mentions_d.type == 3] = 't'
day_mentions_d.type[day_mentions_d.type == 4] = 'c'

percent_type = pd.DataFrame({'contestant':[], 'tipo_1':[], 'tipo_2':[], 'tipo_3':[], 'tipo_4':[]})

for contestant in by_user.indices:
    
    pivote_type_tw = day_mentions_d[day_mentions_d.id_tw_user_screen_name == contestant].pivot_table(values='id_str_msg', index = ['date'], columns = 'type', aggfunc = len)
    #actualizamos nan a 0
    pivote_type_tw = pivote_type_tw.fillna(0)
    pivote_type_tw.columns = ['u','d','t','c']
    #definimos series   
    serie_1 = pd.Series(pivote_type_tw.u)
    serie_2 = pd.Series(pivote_type_tw.d)
    serie_3 = pd.Series(pivote_type_tw.t)
    serie_4 = pd.Series(pivote_type_tw.c)
            
    percent_type.loc[len(percent_type)]=[contestant, sum(pivote_type_tw.iloc[:,0]), sum(pivote_type_tw.iloc[:,1]), sum(pivote_type_tw.iloc[:,2]), sum(pivote_type_tw.iloc[:,3])]    
    
    #opción varias ventanas    
    fig, ax = plt.subplots(figsize=(12, 4))
    n, = plt.plot(serie_1, color = '#e41a1c', label = ('Normales: '  + str(int((percent_type.tipo_1[percent_type.contestant == contestant])/(percent_type[percent_type.contestant == contestant].sum(axis=1))*100)) + '%'))
    r, = plt.plot(serie_2, color = '#377eb8', label = ('Retweeted: ' + str(int((percent_type.tipo_2[percent_type.contestant == contestant])/(percent_type[percent_type.contestant == contestant].sum(axis=1))*100)) + '%'))
    q, = plt.plot(serie_3, color = '#4daf4a', label = ('Quoted: ' + str(int((percent_type.tipo_3[percent_type.contestant == contestant])/(percent_type[percent_type.contestant == contestant].sum(axis=1))*100)) + '%'))
    rq, = plt.plot(serie_4, color = '#984ea3', label = ('Ret + Quo. :' + str(int((percent_type.tipo_4[percent_type.contestant == contestant])/(percent_type[percent_type.contestant == contestant].sum(axis=1))*100)) + '%'))
    
    ax.set_ylim(ymin=-10) 
    
    ax.xaxis.set_minor_locator(dates.WeekdayLocator(byweekday=TH))
    ax.xaxis.set_minor_formatter(dates.DateFormatter('%d\n%a'))
    ax.xaxis.grid(True, which="minor")
    ax.yaxis.grid()
    ax.xaxis.set_major_locator(dates.MonthLocator())
    ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n%b\n%Y'))
    plt.title(contestant)
    
       
    plt.legend(handles=[n,r,q,rq], fontsize = 9, loc = 1, ncol = 4)
    plt.tight_layout()
            
    im = plt.imread(path + '\img\\fotos\\' + contestant + '.JPG')
    plt.axes([0.03, 0.75, 0.25, 0.25])
    plt.imshow(im)
    plt.axis('off')
            
    plt.savefig(path + '\img\user_mentions\\type\\'+ contestant+'_type.png')