# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 22:26:46 2019

@author: paula.guarido
"""
import os
import matplotlib.pyplot as plt
from matplotlib import dates
plt.style.use('ggplot')
import pandas as pd
import csv
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU


path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')

#cargar tablas
tw_user_mentions = pd.read_csv(path + '\procesados\csv\\tw_user_mentions.csv', index_col = False, dtype={'id_str_msg':str})
tw_user_mentions = tw_user_mentions.set_index('id_user_mention')

tw_hashtags = pd.read_csv(path + '\procesados\csv\\tw_hashtags.csv', index_col = False, dtype={'id_str_msg':str} )
tw_hashtags = tw_hashtags.set_index('id_hashtag')

tw_msg = pd.read_csv(path + '\procesados\csv\\tw_msg.csv', index_col = False, dtype={'id_str_msg':str})


day_mentions = pd.DataFrame()
day_mentions = tw_user_mentions[tw_user_mentions.id_tw_user_screen_name != '0']
day_mentions['id_tw_previous'] = day_mentions.index
day_mentions['id_day_mention'] = range(1, len(day_mentions) + 1)
day_mentions = day_mentions.set_index('id_day_mention')
day_mentions['from'] = 'user_mentions'

day_mentions_aux = pd.DataFrame()
day_mentions_aux = tw_hashtags[tw_hashtags.id_tw_user_screen_name != '0']
day_mentions_aux['id_tw_previous'] = day_mentions_aux.index
day_mentions_aux['id_day_mention'] = range(1, len(day_mentions_aux) + 1)
day_mentions_aux = day_mentions_aux.set_index('id_day_mention')
day_mentions_aux['from'] = 'hashtags'
day_mentions_aux = day_mentions_aux.drop('text', axis = 1)

#unimos los df: day_mentions y day_metions_aux (menciones y hashtags)
day_mentions = day_mentions.append(day_mentions_aux)
#generamos la key
day_mentions['id_day_mention'] = range(1, len(day_mentions) + 1)
day_mentions = day_mentions.set_index('id_day_mention')

date = pd.DataFrame()
date = tw_msg[['id_str_msg','date']]
date = date.set_index('id_str_msg')

#unimos los df: day_mentions y date (menciones, hahstags y fecha)
day_mentions_d = pd.DataFrame()
day_mentions_d = day_mentions.join(date, on='id_str_msg')

day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'albert_iceman89'] = 'ALBERT'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == '_ISABELPANTOJA'] = 'ISABEL'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == '_CarlosLozano'] = 'CARLOS'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'omarmontesSr'] = 'OMAR'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'chgarciacortes'] = 'CHELO'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'hoyosmoni'] = 'MONICA'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'ColateNicolasVN'] = 'COLATE'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'lidiasantosoffi'] = 'LIDIA'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'OtoVans'] = 'OTO'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'MahiMasegosa'] = 'MAHI'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'violetamngr03'] = 'VIOLETA'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'fabioeltano'] = 'FABIO'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'dakotanadia80'] = 'DAKOTA'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'lolyalvarez4u'] = 'LOLY'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'AnethStyle_'] = 'ANETH'
day_mentions_d.id_tw_user_screen_name[day_mentions_d.id_tw_user_screen_name == 'JonathanPiquer'] = 'JONATHAN'


# Guardamos la salida en formato excel y csv
day_mentions_d.to_csv(path  + '\procesados\csv\\day_mentions_d.csv', encoding = 'utf-8')
day_mentions_d.to_excel(path +  '\procesados\excel\\day_mentions_d.xlsx')

#day_mentions_d = pd.read_csv(path + '\procesados\csv\\day_mentions_d.csv', index_col = False, dtype={'id_str_msg':str})
#day_mentions_d = day_mentions_d.set_index('id_day_mention')

by_user = day_mentions_d.groupby('id_tw_user_screen_name')
by_date = by_user['date']

#opcion para imprimirlos en una ventana

percent_mentions = pd.DataFrame({'contestant':[], 'perc':[]})
for contestant in by_user.indices:
    cont = pd.Series(by_user.get_group(contestant).groupby('date').size())
    cont.index = pd.to_datetime(cont.index)
    percent = round((float(cont.sum())/float(day_mentions_d.id_tw_user_screen_name.count()))*100, 3)
      
    percent_mentions.loc[len(percent_mentions)]=[contestant, percent]
    
    #opcion1 ventana
    #plt.subplot(16,1,n)
    #opcion varias ventanas
    # graficos concursantes
    fig, ax = plt.subplots(figsize=(12,4))
    plt.plot(cont)
    
    ax.xaxis.set_minor_locator(dates.WeekdayLocator(byweekday=TH))
    #ax.xaxis.set_minor_locator(dates.WeekdayLocator(byweekday=(range(7))))
  
    ax.xaxis.set_minor_formatter(dates.DateFormatter('%d\n%a'))
    ax.xaxis.grid(True, which='minor')
    ax.xaxis.grid()
    ax.xaxis.set_major_locator(dates.MonthLocator())
    ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n%b\n%Y'))
    plt.title(contestant)
    plt.ylabel(u'Nº de menciones y hashtags')
    plt.tight_layout()
    im = plt.imread(path + '\img\\fotos\\' + contestant + '.JPG')
    plt.axes([0.03, 0.75, 0.25, 0.25])
    plt.imshow(im)
    plt.axis('off')
    
    plt.text(0, 170, str(percent).replace('.',',') + '% del total', horizontalalignment='left', verticalalignment='center', fontsize=20, alpha=.5)
    plt.savefig(path + '\img\user_mentions\\'+contestant+'.png')
    #n+=1
     
    # grafico porcentajes menciones usurios y hahstags
    percent_mentions.sort_values(by='perc').plot(kind = 'barh', x='contestant', y='perc', legend = False, figsize =(12, 4), color = '#9e00ff', title= 'Porcentaje de menciones y hashtags asociados')
    plt.ylabel('Concursantes')
    plt.xlabel('Tanto %')
    plt.tight_layout()
    plt.savefig(path + '\img\user_mentions\\porcentajes.png')
    


