# -*- coding: utf-8 -*-
"""
Created on Sun Jun  2 00:24:51 2019

@author: paula.guaridom
"""

import sys
import os
import json
import operator
import csv
import pandas as pd
import time

path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')
#path = 'C:\Axians\OneDrive - VINCI Energies\Axians\UNIR\Asignaturas\TFG\ficherosdatos'
#os.chdir(path)

tw_hashtag_list = pd.read_excel(path + '\procesados\excel\hashtags_usuarios.xlsx', 'hashtags')
tw_hashtag_list['hashtag'] = tw_hashtag_list['hashtag'].str.lower()

try:
    fichero = open(path + '\procesados\procesado.json', 'r')
    print 'fichero leido'
    #fichero.close()
except IOError:
    print 'error de lectura 1'

tweets_data = []

for tweet_line in fichero:
    tweet = json.loads(tweet_line)
    tweets_data.append(tweet)

tweet_line = None

#Función para asignar el tipo de tweet en las tablas tw_hashtags y tw_msg
def type_tw(tweet):
    #caso1
    if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(1)
    #caso2
    elif 'quoted_status' not in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(2)
    #caso3
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(3)
    #caso4
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(4)

print 'tipo de tweet asignado'
#TABLA tw_msg
tw_msg = pd.DataFrame()
tw_msg['id_str_msg'] = map(lambda tweet: tweet['id_str'], tweets_data)
tw_msg['timestamp_ms'] = map(lambda tweet: tweet['timestamp_ms'], tweets_data)
tw_msg['lang'] = map(lambda tweet: tweet['lang'], tweets_data)
tw_msg['id_str_user'] = map(lambda tweet: tweet['user']['id_str'], tweets_data)
#estos dos son siempre 0
tw_msg['retweet_count'] = map(lambda tweet: tweet['retweet_count'], tweets_data)
tw_msg['favorite_count'] = map(lambda tweet: tweet['favorite_count'], tweets_data)
tw_msg['retweet_count_rt'] = map(lambda tweet: tweet['retweeted_status']['retweet_count'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['retweet_count_fv'] = map(lambda tweet: tweet['retweeted_status']['favorite_count'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['retweeted_status_id_str'] = map(lambda tweet: tweet['retweeted_status']['id_str'] if 'retweeted_status' in tweet.keys() else None, tweets_data)
tw_msg['quoted_status_id_str'] = map(lambda tweet: tweet['quoted_status']['id_str'] if 'quoted_status' in tweet.keys() else None, tweets_data)
tw_msg['date'] = map(lambda tweet: time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')),tweets_data)
#Recogemos el texto del tweet normal RT o QT
"""
1. Normal. sin RT o QE --> texto del tweet --> tweet['text']
2. RT. sin modificar el original --> texto del RT --> tweet['retweeted_status']['text']
3. QE. modificando el original --> 2 textos, tweet['quoted_status']['text'], tweet['text']
4. RT+QE. RT de un QE --> igual que 3
"""
tw_msg['text_m'] = map(lambda tweet: tweet['text'] if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys() else '', tweets_data)
tw_msg['text_rt'] = map(lambda tweet: tweet['retweeted_status']['text'] if 'retweeted_status' in tweet.keys() else '', tweets_data)
tw_msg['text_qt'] = map(lambda tweet: tweet['quoted_status']['text'] if 'quoted_status' in tweet.keys() else '', tweets_data)
# Generamos el texto a enviar a MC
tw_msg['text_mc'] = tw_msg['text_m'] +' '+ tw_msg['text_rt'] +' '+ tw_msg['text_qt']
# Indicamos si es un tweet a MC
##tw_msg['is_mc'] = False
##tw_msg.is_mc.ix[tw_msg['id_str_msg'].isin(tw_hashtags.id_str_msg[tw_hashtags.id_tw_user_screen_name != '0'])] = True
###tw_msg.is_mc.ix[tw_msg['id_str_msg'].isin(tw_user_mentions.id_str_msg[tw_user_mentions.id_tw_user_screen_name != '0'])] = True
#Tipo tw
tw_msg['type'] = map(lambda tweet: type_tw(tweet), tweets_data)

print 'tabla tw_msg_creada'
# Guardamos la salida en formato excel y csv
tw_msg.to_excel(path + '\procesados\excel\\tw_msg.xlsx')
tw_msg.to_csv(path + '\procesados\csv\\tw_msg.csv', encoding = 'utf-8')

print 'tabla tw_msg_a_excel'
#Probamos que se abren correctamente
#tw_msg = pd.read_csv(path + '\procesados\csv\tw_msg.csv', index_col= False, dtype={'id_str_msg':str, 'id_str_user':str} )
#tw_msg =tw_msg.set_index('id_msg')

#TABLA tw_user
tw_user = pd.DataFrame()
tw_user['id_str_user'] = map(lambda tweet: tweet['user']['id_str'], tweets_data)
tw_user['name'] = map(lambda tweet: tweet['user']['name'], tweets_data)
tw_user['screen_name'] = map(lambda tweet: tweet['user']['screen_name'], tweets_data)
tw_user['location'] = map(lambda tweet: tweet['user']['location'], tweets_data)
tw_user['followers_count'] = map(lambda tweet: tweet['user']['followers_count'], tweets_data)
tw_user['friends_count'] = map(lambda tweet: tweet['user']['friends_count'], tweets_data)
tw_user['lang'] = map(lambda tweet: tweet['user']['lang'], tweets_data)
tw_user['geo_enabled'] = map(lambda tweet: tweet['user']['geo_enabled'], tweets_data)
tw_user['coordinates'] = map(lambda tweet: tweet['coordinates'], tweets_data)
# Borrar usu no relacionados con tw seleccionados??
tw_user = tw_user.drop_duplicates(subset = 'id_str_user', keep='last')

print 'tabla tw_user_creada'
# Guardamos la salida en formato excel y csv
tw_user.to_csv(path +  '\procesados\csv\\tw_user.csv', encoding = 'utf-8')
tw_user.to_excel(path +  '\procesados\excel\\tw_user.xlsx')

print 'tabla tw_user_a_excel'
#Probamos que se abren correctamente
#tw_user = pd.read_csv(path + '\procesados\csv\tw_user.csv', index_col= False, dtype={ 'id_str_user':str} )
#tw_user =tw_user.set_index('id_str')


#TABLA tw_contestant
contestant = {
        'id_tw_user_screen_name':['_ISABELPANTOJA','_CarlosLozano','omarmontesSr','chgarciacortes','hoyosmoni','ColateNicolasVN','lidiasantosoffi','OtoVans','MahiMasegosa', 'violetamngr03','fabioeltano','dakotanadia80','albert_iceman89','lolyalvarez4u','AnethStyle_','JonathanPiquer'],
        'contestant':['ISABEL PANTOJA','Carlos Lozano','Omar Montes','Chelo García Cortés','Monica Hoyos','Nicolas Vallejo-Nage','Lidia Santos','taxxxxiiista, te gusta?','Mahi Masegosa','Violeta Mangriñan','fabio colloricchio','dakota tarraga','Albert Álvarez','Loly Álvarez SUPERVIVIENTE 2019','AnethStyle.com','Jonathan Piqueras'],
        'contestant_mention':['4645','242','586','1943','5336','717','75','102','26','3769','17','46','446','502','81','1411']
        }
tw_contestant = pd.DataFrame(contestant)
contestant = None


#TABLA tw_user_mentions
# Funcion para asignar concursante en la tabla tw_user_mentions
def asign_contestant_um(um): 
    try:
        return (tw_contestant[tw_contestant.id_tw_user_screen_name == um].iloc[0].id_tw_user_screen_name)
    except:
        return(0)
        
tw_user_mentions = pd.DataFrame()

for tweet in tweets_data:
    
    if type_tw(tweet) == 1:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     
    elif type_tw(tweet) == 2:
         user_mentions = tweet['retweeted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     
    elif type_tw(tweet) == 3:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                  
         user_mentions = tweet['quoted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
        
    elif type_tw(tweet) == 4:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     
         user_mentions = tweet['quoted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)

id_user_mentions = range(1, len(tw_user_mentions) + 1)
tw_user_mentions['id_user_mention'] = id_user_mentions
tw_user_mentions = tw_user_mentions.set_index('id_user_mention')

# Guardamos la salida en formato excel y csv
tw_user_mentions.to_csv(path +  '\procesados\csv\\tw_user_mentions.csv', encoding = 'utf-8')
tw_user_mentions.to_excel(path +  '\procesados\excel\\tw_user_mentions.xlsx')

#TABLA tw_hashtags
# Funcion para asignar concursante en la tabla de hashtags
def asign_contestant(ht):
    try:
        return(tw_hashtag_list[tw_hashtag_list.hashtag == ht].iloc[0].contestant_01)
    except:
        return 0
    
tw_hashtags = pd.DataFrame()

for tweet in tweets_data:
   
    if type_tw(tweet) == 1:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
    elif type_tw(tweet) == 2:
         hashtags = tweet['retweeted_status']['entities']['hashtags']
         for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
    elif type_tw(tweet) == 3:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
        hashtags = tweet['quoted_status']['entities']['hashtags']
        for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
    elif type_tw(tweet) == 4:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
        hashtags = tweet['quoted_status']['entities']['hashtags']
        for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    
id_hashtag = range(1, len(tw_hashtags) + 1)
tw_hashtags['id_hashtag'] = id_hashtag
tw_hashtags = tw_hashtags.set_index('id_hashtag')

# Guardamos la salida en formato excel y csv
tw_hashtags.to_csv(path +  '\procesados\csv\\tw_hashtags.csv', encoding = 'utf-8')
tw_hashtags.to_excel(path +  '\procesados\excel\\tw_hashtags.xlsx')

#Probamos que se abren correctamente
#tw_hashtags = pd.read_csv(path + '\procesados\csv\tw_hashtags.csv', index_col= False, dtype={ 'id_str_msg':str} )
#tw_hashtags =tw_hashtags.set_index('id_hashtag')




        
    
    