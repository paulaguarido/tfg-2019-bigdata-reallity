# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 20:36:22 2019

@author: paula.guarido
"""

#cambio quitar
import sys
import os
import json
import operator
import csv
import pandas as pd
import time

path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')
#path = 'C:\Axians\OneDrive - VINCI Energies\Axians\UNIR\Asignaturas\TFG\ficherosdatos'
#os.chdir(path)

tw_hashtag_list = pd.read_excel(path + '\procesados\excel\hashtags_usuarios.xlsx', 'hashtags')
tw_hashtag_list['hashtag'] = tw_hashtag_list['hashtag'].str.lower()

try:
    fichero = open(path + '\procesados\procesado.json', 'r')
    print 'fichero leido'
    #fichero.close()
except IOError:
    print 'error de lectura 1'

tweets_data = []

for tweet_line in fichero:
    tweet = json.loads(tweet_line)
    tweets_data.append(tweet)

tweet_line = None

#Función para asignar el tipo de tweet en las tablas tw_hashtags y tw_msg
def type_tw(tweet):
    #caso1
    if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(1)
    #caso2
    elif 'quoted_status' not in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(2)
    #caso3
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(3)
    #caso4
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(4)

print 'tipo de tweet asignado'

#TABLA tw_hashtags
"""
Para la selección correcta de los hashtags, necesitamos saber como es el tweet
1. Normal. sin RT o QE --> entities del tweet --> tweet['entities']
2. RT. sin modificar el original --> entities del RT --> tweet['retweeted_status']['entities']
3. QE. modificando el original --> 2 entities, tweet['quoted_status']['entities'], tweet['entities']
4. RT+QE. RT de un QE --> igual que 3
"""
# Funcion para asignar concursante en la tabla de hashtags
def asign_contestant(ht):
    try:
        return(tw_hashtag_list[tw_hashtag_list.hashtag == ht].iloc[0].contestant_01)
    except:
        return 0
    
tw_hashtags = pd.DataFrame()
cont = 0

for tweet in tweets_data:
    cont+=1
    print(cont)
    
    if type_tw(tweet) == 1:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo1'
                    
    elif type_tw(tweet) == 2:
         hashtags = tweet['retweeted_status']['entities']['hashtags']
         for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo2'
                    
    elif type_tw(tweet) == 3:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo3_1'
                    
        hashtags = tweet['quoted_status']['entities']['hashtags']
        for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo3_2'
                    
    elif type_tw(tweet) == 4:
        if'entities' in tweet.keys():
            hashtags = tweet['entities']['hashtags']
            for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo4_1'
                    
        hashtags = tweet['quoted_status']['entities']['hashtags']
        for ht in hashtags:
                if ht != None:
                    datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'text':[ht['text']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant(ht['text'].lower().encode('UTF-8'))]
                             }
                    fila = pd.DataFrame(datos)
                    tw_hashtags = tw_hashtags.append(fila)
                    print 'UM tipo1_2'
                    
id_hashtag = range(1, len(tw_hashtags) + 1)
tw_hashtags['id_hashtag'] = id_hashtag
tw_hashtags = tw_hashtags.set_index('id_hashtag')

print 'tabla tw_hashtags_creada'

# Guardamos la salida en formato excel y csv
tw_hashtags.to_csv(path +  '\procesados\csv\\tw_hashtags.csv', encoding = 'utf-8')
tw_hashtags.to_excel(path +  '\procesados\excel\\tw_hashtags.xlsx')

print 'tabla tw_hashtags_a_excel'