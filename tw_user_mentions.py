# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 20:18:14 2019

@author: paula.guarido
"""

import sys
import os
import json
import operator
import csv
import pandas as pd
import time

path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')
#path = 'C:\Axians\OneDrive - VINCI Energies\Axians\UNIR\Asignaturas\TFG\ficherosdatos'
#os.chdir(path)

tw_hashtag_list = pd.read_excel(path + '\procesados\excel\hashtags_usuarios.xlsx', 'hashtags')
tw_hashtag_list['hashtag'] = tw_hashtag_list['hashtag'].str.lower()

try:
    fichero = open(path + '\procesados\procesado.json', 'r')
    print 'fichero leido'
    #fichero.close()
except IOError:
    print 'error de lectura 1'

tweets_data = []

for tweet_line in fichero:
    tweet = json.loads(tweet_line)
    tweets_data.append(tweet)

tweet_line = None

#Función para asignar el tipo de tweet en las tablas tw_hashtags y tw_msg
def type_tw(tweet):
    #caso1
    if 'quoted_status' not in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(1)
    #caso2
    elif 'quoted_status' not in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(2)
    #caso3
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' not in tweet.keys():
        return(3)
    #caso4
    elif 'quoted_status' in tweet.keys() and 'retweeted_status' in tweet.keys():
        return(4)

print 'tipo de tweet asignado'

contestant = {
        'id_tw_user_screen_name':['_ISABELPANTOJA','_CarlosLozano','omarmontesSr','chgarciacortes','hoyosmoni','ColateNicolasVN','lidiasantosoffi','OtoVans','MahiMasegosa', 'violetamngr03','fabioeltano','dakotanadia80','albert_iceman89','lolyalvarez4u','AnethStyle_','JonathanPiquer'],
        'contestant':['ISABEL PANTOJA','Carlos Lozano','Omar Montes','Chelo García Cortés','Monica Hoyos','Nicolas Vallejo-Nage','Lidia Santos','taxxxxiiista, te gusta?','Mahi Masegosa','Violeta Mangriñan','fabio colloricchio','dakota tarraga','Albert Álvarez','Loly Álvarez SUPERVIVIENTE 2019','AnethStyle.com','Jonathan Piqueras'],
        'contestant_mention':['4645','242','586','1943','5336','717','75','102','26','3769','17','46','446','502','81','1411']
        }
tw_contestant = pd.DataFrame(contestant)
contestant = None

# Funcion para asignar concursante en la tabla tw_user_mentions
def asign_contestant_um(um): 
    try:
        return (tw_contestant[tw_contestant.id_tw_user_screen_name == um].iloc[0].id_tw_user_screen_name)
    except:
        return(0)
        
tw_user_mentions = pd.DataFrame()
cont = 0
for tweet in tweets_data:
    cont+=1
    print(cont)    
    if type_tw(tweet) == 1:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo1'
                     
    elif type_tw(tweet) == 2:
         user_mentions = tweet['retweeted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo2'
                     
    elif type_tw(tweet) == 3:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo3_1'                  
                     
         user_mentions = tweet['quoted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo3_2'
        
    elif type_tw(tweet) == 4:
         if'entities' in tweet.keys():
             user_mentions = tweet['entities']['user_mentions']
             for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo4_1'
                     
         user_mentions = tweet['quoted_status']['entities']['user_mentions']
         for um in user_mentions:
                 if um != None:
                     datos = {'id_str_msg':[tweet['id_str']],
                             'ts':[tweet['timestamp_ms']],
                             'type':[type_tw(tweet)],
                             'id_tw_user_screen_name':[asign_contestant_um(um['screen_name'].encode('UTF-8'))]
                             }
                     fila = pd.DataFrame(datos)
                     tw_user_mentions= tw_user_mentions.append(fila)
                     print 'UM tipo4_2'

id_user_mentions = range(1, len(tw_user_mentions) + 1)
tw_user_mentions['id_user_mention'] = id_user_mentions
tw_user_mentions = tw_user_mentions.set_index('id_user_mention')

print 'tabla tw_user_mentions_creada'
# Guardamos la salida en formato excel y csv
tw_user_mentions.to_csv(path +  '\procesados\csv\\tw_user_mentions.csv', encoding = 'utf-8')
tw_user_mentions.to_excel(path +  '\procesados\excel\\tw_user_mentions.xlsx')
#tw_user_mentions.to_excel(path +  '\procesados\csv\tw_user_mentions.csv', encoding = 'utf-8')

print 'tabla tw_user_mentions_a excel'
