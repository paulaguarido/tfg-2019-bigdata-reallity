# -*- coding: utf-8 -*-
"""
Created on Sun Jul 28 16:09:03 2019

@author: paula.guarido
"""

import os
import matplotlib.pyplot as plt
from matplotlib import dates
plt.style.use('ggplot')
import pandas as pd
import csv


path = os.path.join('C:\Axians','OneDrive - VINCI Energies','Axians','UNIR','Asignaturas','TFG','files')


tw_msg = pd.read_csv(path + '\procesados\csv\\tw_msg.csv', index_col = False, dtype={'id_str_msg':str})
#tw_msg = tw_msg.set_index('id_str_msg')

date = pd.DataFrame()
date = tw_msg[['id_str_msg','date']]
#date = date.set_index('id_str_msg')

#day_mentions_d = pd.DataFrame()
#day_mentions_d = day_mentions.join(date, on='id_str_msg')
#day_mentions_d = day_mentions_d.drop_duplicates()

date.to_csv(path  + '\procesados\csv\\date.csv', encoding = 'utf-8')
date.to_excel(path +  '\procesados\excel\\date.xlsx')
